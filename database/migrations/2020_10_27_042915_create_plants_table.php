<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plants', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->string('hostname');
            $table->timestamp('birthday', 0)->nullable();
            $table->string('license');
            $table->boolean('enabled')->default(true);
            $table->integer('user_id')->nullable();
            $table->integer('delay_seconds')->nullable();
            $table->integer('next_run_in_minutes')->nullable();
            $table->timestamp('last_ran_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plants');
    }
}
