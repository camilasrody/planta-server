<?php

namespace Database\Factories;

use App\Models\Plant;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use Faker\Factory as Faker;

class PlantFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Plant::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name,
            'hostname' => $this->faker->localIpv4,
            'birthday' => null,
            'license' => Str::random(10),
            'delay_seconds' => rand(1, 10),
            'next_run_in_minutes' => rand(1, 60),
            'last_ran_at' => (rand(0, 20) > 10) ? now() : null,
            'enabled' => false,
        ];
    }
}
