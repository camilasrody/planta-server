<?php

namespace App\Http\Controllers;

use App\Models\Plant;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Requests;

class PlantController extends Controller
{
    public function show($id)
    {
        $plant = Plant::firstWhere('id', $id);

        $plant->getInfo();
        $plant->indicators = $plant->indicators;

        return json_encode($plant);
    }

    public function index()
    {
        $plants = Plant::all();

        foreach ($plants as $k => $plant) {
            $plant->getInfo();
            $plant->indicators = $plant->indicators;
        }

        return json_encode($plants);
    }

    /**
     * water the plant only if the plant need's irrigation
     *
     * @param $id
     * @return false|string
     */
    public function checkIrrigation($id)
    {
        $plant = Plant::firstWhere('id', $id);

        $plant->sensorResult = $plant->handleIrrigation();

        return json_encode($plant);
    }

    public function userShow($id)
    {
        $user = User::firstWhere('id', $id);

        return json_encode($user);
    }

    public function license(Request $request)
    {
        $license = $request->input('license');
        $plant = Plant::firstWhere('license', $license);

        return json_encode($plant->user());
    }

    /**
     * Store a user plant.
     *
     * @param Request $request
     * @return Response
     */
    public function userStore(Request $request)
    {
        $id = $request->input('id', 1);
        Log::warning("id=" . $id);
        $user = User::findOrFail($id);

//        $name = $request->file('profile_photo_url')->getClientOriginalName();
//        print_r(['all_data' => $request->all('profile_photo_url'),
//                 'request_files' => $request->allFiles(),
//                 '$_FILES' => $_FILES,
////                 'valid'    => $request->file('profile_photo_url')->isValid()
//        ]);
//        exit;
        $profile_photo_url = '';
        $request->replace($request->all());
        if ($request->hasFile('profile_photo_url')) {
            Log::warning("image recived");
//            $profile_photo_url = request()->file('profile_photo_url')->store('public/images');
            $image = $request->file('photo');
            $profile_photo_url = $user->updateProfilePhoto($image);
        }else{
            Log::warning("no image found on profile_photo_url");
            return false;
        }

        $user->update(array_merge($request->all(), [
            'profile_photo_url' => $profile_photo_url
        ]));

        return json_encode($user);
    }

    /**
     * Store a user plant.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $id = $request->input('id', 1);
        $plant = Plant::findOrFail($id);

        $plant->update($request->all());

        return json_encode($plant);
    }
//    public function updateRunFrequency($id, $minutes){
//        $plant = Plant::firstWhere('id', $id);
//
//        $plant->update([
//            'next_run_in_minutes' => $minutes
//        ]);
//
//        return json_encode($plant);
//    }
}
