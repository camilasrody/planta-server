<?php

namespace App\Http\Controllers;

use App\Models\Log;
use Illuminate\Http\Request;

class LogController extends Controller
{

    public function index(Request $request)
    {
        $id = $request->input('id', 1);
        $logs = Log::where('plant_id', $id)->get();


        return json_encode($logs);
    }

}
