<?php

namespace App\Models;

use App\Models\Enums\CheckStatus;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
use Requests;

class Plant extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'hostname',
        'birthday',
        'license',
        'enabled',
        'last_ran_at',
        'board',
        'enabled',
        'next_run_in_minutes',
        'delay_seconds'
    ];

    public $dates = [
        'last_ran_at', 'next_check_at'
    ];
    /**
     * @var bool
     */
    public $sensorResult = -1;

    /**
     * Get the indicators for the plant.
     */
    public function indicators()
    {
        return $this->hasMany('App\Models\Indicator');
    }
    /**
     * Get the logs for the plant.
     */
    public function logs()
    {
        return $this->hasMany('App\Models\Log');
    }

    /**
     * Get the user that owns the plant.
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    private function aggregateIndicators() : bool
    {
        $res = true;
        $indicator = new Indicator([
            'type' => 'humidity',
            'value' => $this->board['humidity'],
        ]);
        $res = $this->indicators()->save($indicator);

        $indicator = new Indicator([
            'type' => 'temperature',
            'value' => $this->get_current_condition(),
        ]);
        return $res && $this->indicators()->save($indicator);
    }

    private function registerActivity($message = '')
    {
        $log = new \App\Models\Log([
            'action' => $message,
        ]);
        return $this->logs()->save($log);
    }

    public function requestArduino($uri = "/"): bool
    {
        try {
            Log::debug("requesting http://$this->hostname$uri");
            $request = Requests::get("http://$this->hostname$uri", [], [
                "timeout" => 120 // default: 10 seconds (too short).
            ]);
            $this->board = json_decode((string)$request->body, true);
        }catch (\Requests_Exception $exception){
            $this->error = $exception->getMessage();
            Log::error("$uri - $this->error");
            return false;
        }
        return true;
    }

    public function getInfo():bool
    {
        $infoResponse = $this->requestArduino("/info.json");
        if ( $this->error ){
            return false;
        }
        return $infoResponse;
    }

    public function irrigate()
    {
        $irrigateResponse = $this->requestArduino("/irrigate.json");
        if ( $this->error ){
            return false;
        }
        return $irrigateResponse;
    }

    /**
     * Return true when it should irrigate
     * @return bool
     */
    public function handleIrrigation()
    {
        // Observa
        $this->getInfo();
        if ( $this->error ){
            return false;
        }

        // remember this run as last
        $p = Plant::findOrFail($this->id);
        $p->update([
            'last_ran_at' => Carbon::now()
        ]);
//        $this->update([
//            'last_ran_at' => Carbon::now()
//        ]);

        // Registra
        $this->aggregateIndicators();

        if ( $this->shouldIrrigate() ){
            $this->sensorResult = $this->irrigate();
            if ( $this->error ){
                Log::error("plant $this->id: $this->error");
                return false;
            }
            Log::debug("plant $this->id humidity: " . $this->board['humidity'] . "% manualHumidity: ". $this->board['manualHumidity'] );
            $this->registerActivity("Planta regada. Umidade atual da planta está " . $this->board['humidity'] . "%");

//            Log::debug(json_encode($this->board));
//            if ( false == $this->board['result'] ) {
//                Log::info("board retured that plant $this->id do not need water.");
//            }

            return true;
        }

        Log::info("plant $this->id should not be irrigated.");
        $this->registerActivity("Planta já está umida, não foi irrigada. Umidade atual da planta está " . $this->board['humidity'] . "%");
        return false;
    }

    private function shouldIrrigate()
    {
        //base on weater and history
        return true;
    }

    public function shouldRun(): bool
    {
        if (! $this->enabled) {
//            Log::info("Plant $this->id should not run because it is disabled.");
            return false;
        }

        if (is_null($this->last_ran_at)) {
            Log::info("Plant $this->id should run because it has not been run yet.");
            return true;
        }

        $isAfterUserHasConfigured = ! $this->last_ran_at
            ->addMinutes($this->next_run_in_minutes)
            ->isFuture();

        if( $isAfterUserHasConfigured ) {
            $this->last_ran_at = Carbon::now();
        }

        $runText = ( $isAfterUserHasConfigured ) ? 'will run' : 'will not run';
        Log::info("Plant $this->id $runText. Because user has configured to run every $this->next_run_in_minutes minutes. last_ran_at=" . $this->last_ran_at);
        return $isAfterUserHasConfigured;
    }

    public function scopeHealthy($query)
    {
        return $query->where('status', CheckStatus::SUCCESS);
    }

    public function scopeUnhealthy($query)
    {
        return $query->where('status', '!=', CheckStatus::SUCCESS);
    }

    function get_current_condition() {
        $weather = $this->get_weather();
        return $weather['current_condition'][0]['temp_C'];
    }

    function get_weather() {
        $request = Requests::get("http://v3.wttr.in/Florianopolis.json?lang=pt&format=j1", [], [
            "timeout" => 20
        ]);
        return json_decode((string)$request->body, true);
    }
}
