<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Indicator extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['type', 'value'];

    /**
     * Get the plant that owns the indicator.
     */
    public function plant()
    {
        return $this->belongsTo('App\Models\Plant');
    }
}
