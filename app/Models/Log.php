<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['action'];


    /**
     * Get the plant that owns the log.
     */
    public function plant()
    {
        return $this->belongsTo('App\Models\Plant');
    }
}
