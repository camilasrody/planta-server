<?php

namespace App\Console\Commands;

use App\Models\Plant;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class Irrigate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'irrigate:routine';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'main I/O irrigate routine loop';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
//        $user = User::find(1);
        $plants = Plant::all();
        Log::info("Monitoring started for ".$plants->count()." plants.");

        foreach ($plants as $plant) {
            if( $plant->shouldRun() ) {
                $plant->handleIrrigation();
            }
//            $log = new Log();
//            $log->action = "Bomba dagua $plant->pumps['statusBomba'] e solenoide $plant->pumps['statusSolenoide']";
//            $log->save();
        }

        return 0;
    }

}
