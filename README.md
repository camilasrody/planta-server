<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>


## Planta-Laravel
### pt-br

Este é um projeto de software destino acadêmico relacionado ao trabalho de conclusão de curso (**TCC**) de Sistemas de informação.
O tema do projeto aborda sustentabilidade, automação e aplicação web/app.
Automação de rega para manter a saúde de temperos e hortaliças.

Nele contém lógicas elaboradas para enviar comandos e interagir com a automação.
Contém:
- Criação de Web server para receber as informações.
- lógica para regar duas vezes ao dia.
- lógica para agendar horário de irrigação.
- lógica para criar histórico relacionado ao usuário.
- lógica para controle de dados utilizando banco de dados MariaDB.
- lógica para orquestrar a umidade e tomar notificar o usuário.

### eng.
This is an academic destination software project related to the conclusion work of Information Systems.
The project's theme addresses sustainability, automation and web / app application.
Watering automation to maintain the health of spices and vegetables.  
It contains logic designed to interact, send commands and interact with automation.
Contains:
- Web server creation to receive information.
- logic for watering twice a day.
- logic to schedule irrigation schedule.
- logic to create user-related history.
- data control logic using MariaDB database.
- logic to orchestrate humidity and take to notify the user.


## Troubleshooting

Você pode limpar o cache do Laravel com estes comandos.
 
`` 
php artisan clear-compiled
php artisan config:clear
php artisan config:cache
php artisan optimize
composer dump-autoload
``

Se você receber este erro  `Uncaught Error: Class 'Illuminate\Foundation\Application' not found`, execute para resolver.

``
composer install --no-scripts
composer update --no-scripts

composer dump-autoload
``
