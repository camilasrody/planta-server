<?php

use App\Http\Controllers\LogController;
use App\Http\Controllers\PlantController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Laravel\Fortify\Http\Controllers\ProfileInformationController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/plant/{id}', [PlantController::class, 'show']);
Route::get('/plants', [PlantController::class, 'index']);
Route::get('/plant/check-irrigation/{id}', [PlantController::class, 'checkIrrigation']);
//Route::get('/plant/update-run-frequency/{id}/{minutes}', [PlantController::class, 'updateRunFrequency']);
Route::put('/plant/{id}', [PlantController::class, 'store']);
Route::get('/records/{id}', [LogController::class, 'index']);
Route::get('/user/{id}', [PlantController::class, 'userShow']);
Route::put('/user/{id}', [PlantController::class, 'userStore']);
Route::post('/license', [PlantController::class, 'license']);
